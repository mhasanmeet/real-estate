## Real Estate and Housing Accommodations System

## Credentials
* Admin
Sadia
123456

Tarin
123456

* user
saida@email.com
123456

tarin@email.com
123456

* agent
agent1@mail.com
123456

* Builder
builder@mail.com
123456

## Project Images

![Home](./images/project-home.png)
![Properties](./images/project-properties.png)
![Dashboard](./images/project-dashboard.png)

