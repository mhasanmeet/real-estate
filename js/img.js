function getImagePreview(event){
    const image = URL.createObjectURL(event.target.files[0]);
    const getImgPrvw = document.getElementById('imagePreview');
    getImgPrvw.style.display = "block";
    getImgPrvw.src = image;
}